#!/usr/bin/env python3
# -*- coding:utf-8 -*-
import pandas as pd
from Genero import Genero
from Peliculas import Pelicula
from Actor import Actor
import os

def preguntar_ano():

	print ("Procure ingresar un año en el cual se hayan estrenado peliculas")
	year = int(input("Ingrese el año al cual desea acceder: "))
	return year
def filtrar_ano(year):

	data = pd.read_csv("movies.csv")
	peliculas = data[data["year"]== year ]
	peliculas = peliculas.filter(["title","genre","country","language","director","actors"])
	return peliculas


def crear_genero(columna_genero):
	#se crea la lista generos
	generos = []
	#se extrae cada genero existente en el listado
	for index, row in columna_genero.iterrows():
		genero = row["genre"].split(",")
		encontrado = False
		#print (genero)
		for gen in genero:
			gen2 = gen.strip()	
			#print (gen2)		
			
			if not generos:
				obj_genero = Genero()
				obj_genero.set_genero(gen2.capitalize())
				generos.append(gen.capitalize())
				#print("se añadio ",gen2, "a generos")
				break
				
			else: 
				for i in generos:
					if i == gen2:
						encontrado = True
		
						break
						
				if not encontrado: 
					obj_genero = Genero()
					obj_genero.set_genero(gen2.capitalize())
					generos.append(gen2.capitalize())
					#print ("se añadio ",gen2,"a generos")
	#la lista generos tenia un error entonces se filtra para que no hayan elementos repetidos
	generos_filtrados=[]
	filtrado = filter(lambda x : x not in generos_filtrados,generos)
	generos_filtrados.extend(filtrado)
	#print ("se han filtrado los generos")	
	
	return generos_filtrados


def pelicula_genero(peliculas_2019, generos):
		
	for index, row in peliculas_2019.iterrows():
		pelicula = row["title"].capitalize()
		genero = row["genre"].split(",")
		obj_pelicula = Pelicula(pelicula)
		
		for gen in genero:
			gen2 = gen.strip()
			
			for obj in generos:
				gen2 = gen2.capitalize()
				#print (obj, gen2)
				if obj == gen2:
					if obj == "Horror":
						horror.append(pelicula)
						
					if obj == "Crime":
						crimen.append(pelicula)
					
					if obj == "Drama":
						drama.append(pelicula)
						
					if obj == "Action":
						accion.append(pelicula)
						
					if obj == "Adventure":
						aventura.append(pelicula)					
						
					if obj == "Sci-fi":
						ciencia_ficcion.append(pelicula)
						
					if obj == "Biography":
						biografia.append(pelicula)

					if obj == "Comedy":
						comedia.append(pelicula)	

					if obj == "Music":
						musica.append(pelicula)	
						
					if obj == "Romance":
						romance.append(pelicula)									

					if obj == "Thriller":
						suspenso.append(pelicula)
					
					if obj == "Animation":
						animacion.append(pelicula)

					if obj == "War":
						guerra.append(pelicula)

					if obj == "Western":
						occidental.append(pelicula)
						
					if obj == "Mistery":
						misterio.append(pelicula)						

					if obj == "Musical":
						musical.append(pelicula)
						
					if obj == "Family":
						familia.append(pelicula)					
						
					if obj == "Sport":
						deporte.append(pelicula)

					if obj == "History":
						historia.append(pelicula)												
					
					if obj == "Fantasy":
						fantasia.append(pelicula)						
	#print (generos)	
	
	return generos
					
	
def menu_genero(generos):
	
	if not generos:
		print ("Ese año no se estreno ninguna pelicula")
	else:	
		num = 1
		print ("\nLos generos encontrados para ese año son: ")
		for gen in generos:
			print (num,gen)
			num +=1
		print ("A continuacion se mostrara un listado de toda las peliculas del genero escogido que se estrenaron ese año")
		numero = int(input("Ingrese el numero del genero al cual desea acceder: "))
		num = 1
		for gen in generos:
			gen = gen.capitalize()
			#print (gen, num)
			if numero == num:
				#print (gen)
				if gen == "Horror":
					print (horror)
				if gen == "Crime":
					print (crimen)
				if gen == "Drama":
					print (drama)
				if gen == "Action":
					print (accion)
				if gen == "Adventure":
					print (aventura)
				if gen == "Sci-fi":
					print (ciencia_ficcion)
				if gen == "Biography":
					print (biografia)
				if gen == "Comedy":
					print (comedia)
				if gen == "Music":
					print (musica)
				if gen == "Romance":
					print (romance)
				if gen == "Thriller":
					print (suspenso)
				if gen == "Animation":
					print (animacion)
				if gen == "War":
					print (guerra)
				if gen == "Western":
					print (occidental)
				if gen == "Fantasy":
					print (fantasia)
				if gen == "Mystery":
					print (misterio)
				if gen == "Musical":
					print (musical)
				if gen == "Family":
					print (familia)
				if gen == "Sport":
					print (deporte)
				if gen == "History":
					print (historia)
				
			num +=1
		

def actor_pelicula(peliculas_2019):
	#Recorro el data buscando el actor ingresado
	#se crea el objeto actor
	
	nombre = input("Ingrese el nombre del actor el cual quiere ver las peliculas en las que participo: ")								
	nombre = nombre.capitalize()
	numero = 1 
	print (nombre)
	print ("\nLa lista de peliculas en las que actuo ",nombre," se mostrara a continuacion: ")
	for index,row in peliculas_2019.iterrows():
		#print(type(actores))
		pelicula = row["title"].capitalize()
		actores = row["actors"]
		
		if (type(actores) != float):
			actores = actores.split(",")
			#print (actores)
			for actor in actores:
				actor = actor.strip()
				actor = actor.capitalize()
				#print (actor)
				obj_actor = Actor(actor)
				obj_actor.set_nombre(actor)
				#print (obj_actor.get_nombre())
				if nombre == obj_actor.get_nombre():
					print (numero, pelicula)
					numero += 1 
	if (numero == 1):
		print ("El actor ingresado no participo en niuna pelicula el: ", year)		
						

	
if __name__ == "__main__":
	
	
	boton = True
	while boton == True:
		
		
		horror = []
		crimen = []
		drama = []
		accion = []
		aventura = []
		ciencia_ficcion =[]
		biografia =[]
		comedia = []
		musica = []
		romance = []
		suspenso = []
		animacion = []
		guerra = []
		occidental = []
		fantasia = []
		misterio = []
		musical = []
		familia = []
		deporte = []
		historia = []		
		
		print ("\n-----------------Bienvenido---------------------")
		year = preguntar_ano()
		peliculas_2019 = filtrar_ano(year)
		columna_genero = peliculas_2019.filter(["genre"])
		creargenero = crear_genero(columna_genero)
		peliculagenero = pelicula_genero(peliculas_2019,creargenero)
		print ("...............................................")
		print ("Se han filtrado toda las peliculas del: ",year)
		print ("Opcion 1: filtrar peliculas por actor")
		print ("Opcion 2: filtrar peliculas por genero")
		numero = int(input("Presione 1 o 2: "))
		print ("...............................................")
		if numero == 1:	
			mostrar_actor_pelicula = actor_pelicula(peliculas_2019)	
			
		if numero == 2:
			menu = menu_genero(creargenero)
		print ("\nPara reiniciar el programa presione cualquier letra que no sea la s")
		s = input("Para salir del programa presione la s: ")
		s = s.capitalize()
		if s == "S":
			boton = False
	
	
		os.system("clear")

