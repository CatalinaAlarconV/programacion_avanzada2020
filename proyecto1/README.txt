			   					MOVIES ONLINE



Este es un programa fue creado para buscar peliculas en linea ingresando como requisito principal el año de estreno que desea ver, filtrando inmediatamente todas las peliculas correspondiente a ese año ya ingresado, seguidamente el programa da dos opciones de filtrado para la lista de peliculas obtenidas (por actor o por género), donde se pueden tener mejores resultados de búsqueda . Es apto para todo público.

1- Ejecute el programa en la consola.
2- Ingrese el año de las peliculas que desea buscar.
3- Ingrese opción 1 para filtrar por actor u opción 2 para filtrar por género.
4- Si ha ingresado opcion 1, escriba el nombre del actor para filtrar todas las peliculas en donde aparezca.
5- Si ha ingresado opción 2 le aparecerá una lista de géneros que posee este dataset de peliculas. Seleccione un numero y aparecerá acontinuacion el listado de peliculas correspondiente al género seleccionado.
6- Para buscar otras opciones vuelva al inicio ingresando cualquier letra (menos la s)
7- Para finalizar el programa ingrese la letra "s".


CRÉDITOS: Lucas Pavez Calleja
	  Catalina Alarcón Varela


