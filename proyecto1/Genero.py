#!/usr/bin/env python3
# -*- coding:utf-8 -*-
import pandas as pd
from Peliculas import Pelicula


class Genero():
	
	def __init__(self):
		
		self.__genero = None
		self.__peliculas = []
		
		
	def get_genero(self):
		return self.__genero

	def set_genero(self, genero):
		if isinstance(genero, str):
			self.__genero = genero

	def get_peliculas(self):
		return self.__peliculas

	def set_canciones(self, peliculas):
		if isinstance(peliculas, Peliculas):
			self.__peliculas.append(peliculas)
