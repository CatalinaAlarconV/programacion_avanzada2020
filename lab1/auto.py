import random

class tanque:
  def __init__(self, capacidad_maxima = 32):
    self.maximo = capacidad_maxima
    self.actual = 100  # el estanque parte lleno

  def bencina_actual(self):
    return self.maximo * (self.actual / 100.0)

  def consumir(self, porcentaje):
    self.actual = self.actual - porcentaje


class velocimetro:
  def mostrar(self, vel):
    print("la velocidad ahora es ", vel, " km/h")


class motor:
  def __init__(self):
    # al azar entre 0 y 3
    if random.randint(0, 3) == 0:
      self.cilindrada = 1.6
    else:
      self.cilindrada = 1.2


class rueda:
  def __init__(self):
    self.desgaste = 0

  def desgastar(self):
    porcentaje = random.randint(1, 10)
    self.desgaste = self.desgaste + porcentaje

class auto:
  def __init__(self, motor):
    self.set_motor(motor)
    self.set_velocimetro(velocimetro())
    self.set_estanque(tanque(32))

    self.rueda1 = rueda()
    self.rueda2 = rueda()
    self.rueda3 = rueda()
    self.rueda4 = rueda()

    self.encendido = False
    self.recorrido = 0

  def set_motor(self, motor):
    self.motor = motor
  
  def set_velocimetro(self, velocimetro):
    self.velocimetro = velocimetro

  def set_estanque(self, estanque):
    self.estanque = estanque

  def encender(self):
    if self.encendido == False:
      self.encendido = True
      self.estanque.consumir(1)

  def apagar(self):
    if self.encendido == True:
      self.encendido = False

  def info(self):
    if self.encendido == True:
      print "el auto esta encendido y quedan ",self.estanque.bencina_actual(), "litros de bencina"
    else:
      print "el auto esta apagado y quedan ",self.estanque.bencina_actual(), "litros de bencina"

    print "ha recorrido", self.recorrido
    print "el estado de las ruedas es: ", self.rueda1.desgaste, "% ", self.rueda2.desgaste, "%, ", self.rueda3.desgaste, "%, ", self.rueda4.desgaste, "%"

  def avanzar(self):
    velocidad = random.uniform(10, 120)
    tiempo = random.randint(1, 10)
    distancia = velocidad * tiempo

    self.rueda1.desgastar()
    self.rueda2.desgastar()
    self.rueda3.desgastar()
    self.rueda4.desgastar()

    if self.rueda1.desgaste > 90:
      print "el auto no puede avanzar porque se echo a perder la rueda 1"
      self.rueda1 = rueda()
      print "el conductor repara la rueda 1 y queda como nueva"
      
    if self.rueda2.desgaste > 90:
      print "el auto no puede avanzar porque se echo a perder la rueda 2"
      self.rueda2 = rueda()
      print "el conductor repara la rueda 2 y queda como nueva"
      
    if self.rueda3.desgaste > 90:
      print "el auto no puede avanzar porque se echo a perder la rueda 3"
      self.rueda3 = rueda()
      print "el conductor repara la rueda 3 y queda como nueva"
      
    if self.rueda4.desgaste > 90:
      print "el auto no puede avanzar porque se echo a perder la rueda 4"
      self.rueda4 = rueda()
      print "el conductor repara la rueda 4 y queda como nueva"

    #El control de velocidad queda a libre disposicion de implementar y NO se complique con laaceleracion.
    print "el auto avanzo", distancia, "km en", tiempo, "segundos (", velocidad / tiempo, " km/s)"
    self.recorrido = self.recorrido + distancia
